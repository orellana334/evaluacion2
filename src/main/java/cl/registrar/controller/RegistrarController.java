/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.registrar.controller;

import cl.registrar.dao.ClientesJpaController;
import cl.registrar.dao.exceptions.NonexistentEntityException;
import cl.registrar.entity.Clientes;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author AOrellana
 */
@WebServlet(name = "RegistrarController", urlPatterns = {"/RegistrarController"})
public class RegistrarController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RegistrarController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RegistrarController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("registro.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       String nombre = request.getParameter("nombre");
       String apellido =  request.getParameter("apellido");
       String rut = request.getParameter("rut");
       String telefono = request.getParameter("telefono");
       String direccion = request.getParameter("direccion");
       System.out.println("nombre" + nombre);
       System.out.println("apellido" + apellido);
       System.out.println("rut" + rut);
       System.out.println("Telefono" + telefono);
       System.out.println("direccion" + direccion);
       String accion = request.getParameter("accion");       
       
       Clientes cliente = new Clientes();
       cliente.setNombre(nombre);
       cliente.setApellido(apellido);
       cliente.setRut(rut);
       cliente.setTelefono(telefono);
       cliente.setDireccion(direccion); 
       
       ClientesJpaController dao = new ClientesJpaController();
        
        if(accion.equals("modificar")){
        try {
            dao.edit(cliente);
        } catch (Exception ex) {
            Logger.getLogger(RegistrarController.class.getName()).log(Level.SEVERE, null, ex);
        }
       request.setAttribute("cliente", cliente);
       request.getRequestDispatcher("infocliente.jsp").forward(request, response);
    }if(accion.equals("eliminar")){
        
           try {
               dao.destroy(rut);
           } catch (NonexistentEntityException ex) {
               Logger.getLogger(RegistrarController.class.getName()).log(Level.SEVERE, null, ex);
           }
           request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
